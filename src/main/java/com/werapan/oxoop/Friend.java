/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;

/**
 *
 * @author BoM
 */
public class Friend implements Serializable{
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId=1;
    
    public Friend(String name, int age, String tel) {
        this.name = name;
        this.age = age;
        this.tel = tel; 
    }

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception{
        if(age<0){
            throw new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Friend.lastId = lastId;
    }
    
    public static void main(String[] args){
        try {
            Friend f1 = new Friend("pom",70,"0810000");
            Friend f2 = new Friend("Pakat",70,"0810000");
            System.out.println(f1.toString());
            System.out.println(f2);
            f1.setAge(-1);
        } catch (Exception ex) {
            System.out.println("Shit");
        }
                
    }
            
}
